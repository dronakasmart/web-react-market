import React, { Component } from 'react';
import './App.css';
import Goods from './Goods';
import data from './data.json';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">React Market</h1>
        </header>
        <Goods data={data}/>
      </div>
    );
  }
}

export default App;
