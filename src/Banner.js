import React from 'react';
import './Banner.css';
import '../node_modules/bootstrap/dist/css/bootstrap.css';

class Banner extends React.Component {
    constructor (props) {
        super (props);
    }

    render () {
        return (
            <div className='banner-style'>
                <div className='text-banner-span'><span>Реклама на сайте</span></div>
                <img className='img-banner-style' src={this.props.image} alt={this.props.alt} />
                <p><a className='btn btn-success a-banner-style' role='button'>Купить &raquo;</a></p>
            </div>
        );
    }
}

export default Banner;