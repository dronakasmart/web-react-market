import React from 'react';
import './Goods.css';
import Banner from './Banner';
import '../node_modules/bootstrap/dist/css/bootstrap.css';

class Goods extends React.Component {
    constructor (props) {
        super(props);
    }

    render () {
        let stationComponents = this.props.data.map((item) => {
            return <div className='col-lg-4 col-md-6 col-sm-12 col-xs-12' key={item.title}>
                <h2>{item.title}</h2>
                <img className='img-market-style' src={item.src} alt={item.alt}/>
            </div>;
        });

        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-lg-9'>
                        <div className='row'>
                            {stationComponents}
                        </div>
                    </div>
                    <div className='col-lg-3 d-none d-xl-block'>
                        <Banner image={this.props.data[Math.floor(Math.random() * (this.props.data.length - 1))].src}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default Goods;